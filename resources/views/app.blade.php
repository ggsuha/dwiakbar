<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">
    <meta property="og:image" itemprop="image" content="{{ asset('assets/images/image.png') }}">
    <meta property="og:image:alt" content="Akbar & Dwi Wedding Image">
    <meta property="og:description" content="Resepsi diselenggarakan pada Jumat, 06 Januari 2023." />

    <title>Undangan Pernikahan Dwi & Akbar</title>

    <!-- Scripts -->

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/simplelightbox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,400;0,500;0,700;1,300&amp;display=swap"
        rel="stylesheet">
    <link href="{{ asset('assets/css/style_1.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    @routes
    @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
    @inertiaHead
</head>

<body>
    @inertia

    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/plyr.css') }}">
    <script src="{{ asset('assets/js/plyr.js') }}"></script>
</body>

</html>
